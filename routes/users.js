module.exports = function(app, express,auth){
	var router = express.Router();
	var userController = require('../controller/userController');
	var userObj = new userController();
	
	router.get('/',  auth.authenticate(), function(req, res, next){
		console.log(req.user);
		res.json({'success':true});
	});

	router.get('/withoutlogin',  function(req, res, next){
		console.log(req.get('Authorization'));
		res.json({'success':true});
	});

	/*
	router.post('/update', function(req, res, next){
		var access_role = new Array();
		access_role.push('0','1');
		hasAccess(req, res, access_role, next);
	},function(req, res, next){
		userObj.updateUser(req, res, next);
	});
	*/

	router.post('/signup', function(req, res, next){
		userObj.addUser(req, res, next);
	});
	
	app.use('/users', router);
	
}