var userModel = require('../model/userModel');

var multer = require("multer");
var fs = require("fs");

getDetail = function(req, res, next){
	var user_id = req.params.id;
	var search = {'_id':user_id, 'user_type':1};
	userModel.findOne(search).exec(function(err, data){
		if(!err){
			var returnData = {};
			returnData.success = true;
			returnData.data = data;
			res.json(returnData);
		}
		else{
			res.json(err);
		}

	});
}

function randomString(length) {
    var result = '';
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

saveUser = function(adminDetail, callback){
	userModel.addUser(adminDetail, function(addErr, addData){
		var return_data = {};
		if (addErr){
			callback({'data':adminDetail, 'error':addErr}, null);
		} 
		else{
			callback(null, {'success':true});
		}
	});
}


addUser = function(req, res, next){
	var adminDetail = req.body;
	if(typeof req.body.password == "undefined"){
		adminDetail.password = randomString(5);	
	}
	adminDetail.created = Date.now();
	var return_data = {};
	adminDetail.enc_password = userModel.createHash(adminDetail.password);
	saveUser(adminDetail, function(errSave, dataSave){
		if(errSave){
			return_data.success = false;
			return_data.error = errSave;
			res.json(return_data);	
		}
		else{
			res.json({'success':true});	
		}
	})
}

module.exports = function(){
	this.addUser = addUser;
	this.getDetail = getDetail;
}